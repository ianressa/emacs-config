(global-set-key (kbd "C-c C") 'org-capture)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c t") 'org-insert-date)

(setq initial-major-mode 'org-mode)
(setq initial-scratch-message nil)

(package-initialize)

(defvar init-directory (expand-file-name "~/.config/emacs/init"))
(setq custom-file (format "%s/%s" init-directory "custom.el"))

(defun load-init-file (filename)
  (let ((location (format "%s/%s%s" init-directory filename ".el")))
       (or (load location 'noerror)
	   (apply #'message
		  (concat "the init file does not exist: " location)))))

(defun reload-visual ()
  (interactive)
  (load-init-file "visual"))

(load-init-file "packages")
(load-init-file "visual")
(load-init-file "vterm")
(load-init-file "org")
(load-init-file "org-present")
(load-init-file "org-roam")
(load-init-file "irc")
(load-init-file "custom")
