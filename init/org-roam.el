(require 'org-roam)
(setq org-roam-directory (file-truename "~/org-roam"))
(org-roam-db-autosync-mode)

(global-set-key (kbd "C-c n l") 'org-roam-buffer-toggle)
(global-set-key (kbd "C-c n f") 'org-roam-node-find)
(global-set-key (kbd "C-c n i") 'org-roam-node-insert)

(org-roam-setup)
