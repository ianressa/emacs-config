;; melpa and friends
(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)
;;

(setq package-list
      '(
	ace-flyspell
	ace-window
	auctex
	bufler
	dashboard
	elfeed
	flymake-yamllint
	gdscript-mode
	geiser
	go-translate
	helm
	helm-bufler
	lsp-java
	lsp-mode
	magit
	modus-themes
	org-present
	org-roam
	org-superstar
	paredit
	pdf-tools
	plantuml-mode
	system-packages
	visual-fill-column
	vterm
	yaml-mode
	yasnippet
	))

(unless package-archive-contents (package-refresh-contents))
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; ace-window
(require 'ace-window)
(global-set-key (kbd "M-o") 'ace-window)
(setq aw-background nil)
(setq aw-keys '(?a ?r ?s ?t ?d ?h ?n ?e ?i ?o))
;;

;; ace-flyspell
(require 'ace-flyspell)
(global-set-key (kbd "<f8>") 'flyspell-mode)
(global-set-key (kbd "C-<f8>") 'ace-flyspell-jump-word)
(global-set-key (kbd "M-<f8>") 'flyspell-buffer)

(dolist (hook '(org-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))
(dolist (hook '(change-log-mode-hook log-edit-mode-hook))
  (add-hook hook (lambda () (flyspell-mode -1))))
;;

;; AUCTeX
(use-package tex
  :ensure auctex)
(setq TeX-view-program-selection
      '(((output-dvi has-no-display-manager)
	 "dvi2tty")
	((output-dvi style-pstricks)
	 "dvips and gv")
	(output-dvi "xdvi")
	(output-pdf "Okular")
	(output-html "xdg-open")))
;;

;; org-babel
(setq-default org-src-fontify-natively t)
(setq org-startup-with-inline-images t)
(org-babel-do-load-languages
 'org-babel-load-languages '((plantuml . t)))
(setq org-plantuml-jar-path
      (expand-file-name "~/.config/emacs/plantuml/current.jar"))
(setq plantuml-jar-path
      (expand-file-name "~/.config/emacs/plantuml/current.jar"))
;;

;; Bufler
(require 'bufler)
(bufler-mode 1)
(global-set-key (kbd "C-c b") 'bufler-list)
;;

;; ParEdit
(require 'paredit)
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
;; Workaround to make eval-expression work
(define-key paredit-mode-map (kbd "RET") nil)
;;

;; flymake-yamllint
(require 'flymake-yamllint)
(add-hook 'yaml-mode-hook #'flymake-yamllint-setup)
(add-hook 'yaml-mode-hook #'flymake-mode)
;;

;; Geiser
(with-eval-after-load 'geiser-guile
  (add-to-list 'geiser-guile-load-path "~/repos/guix"))
;;

;; YASnippet
(require 'yasnippet)
(yas-global-mode 1)
(setq yas-snippet-dirs
      '("~/.config/emacs/snippets"))
;;

;; dashboard
(require 'dashboard)

(defun my-dashboard-greeting ()
  (let ((hour (nth 2 (decode-time))))
    (format "Good %s, Ian." (if (< hour 12) (if (> hour 3) "morning" "evening") (if (< hour 18) "afternoon" "evening")))))

;(dashboard-setup-startup-hook)
(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))
(setq dashboard-banner-logo-title nil
      dashboard-startup-banner "~/.config/emacs/emacs-dashboard-logo"
      dashboard-set-footer t
      dashboard-footer-messages (list (my-dashboard-greeting))
      dashboard-center-content t
      dashboard-week-agenda t
      dashboard-filter-agenda-entry 'dashboard-no-filter-agenda
      dashboard-agenda-release-buffers t
      dashboard-items '((recents . 5)
			(bookmarks . 5)))

(add-hook 'server-after-make-frame-hook (lambda ()
					  (switch-to-buffer dashboard-buffer-name)
					  (dashboard-mode)
					  (dashboard-insert-startupify-lists)
					  (dashboard-refresh-buffer)))
(add-hook 'dashboard-mode-hook (lambda () (setq-local face-remapping-alist '((default variable-pitch default)))))
(add-hook 'dashboard-before-initialize-hook (lambda () (setq dashboard-footer-messages (list (my-dashboard-greeting)))))
;;

;; helm
(require 'helm)
;(require 'helm-config)

(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))
(global-set-key (kbd "M-x") 'helm-M-x) ; Replace M-x
(global-set-key (kbd "C-s") 'helm-occur) ; Replace I-search
(global-set-key (kbd "C-x C-f") 'helm-find-files) ; Replace C-x C-f

(setq helm-split-window-in-side-p t
      helm-move-to-line-cycle-in-source t
      helm-ff-search-library-in-sexp t
      helm-scroll-amount 8
      helm-ff-file-name-history-use-recentf t
      helm-echo-input-in-header-line t)

(setq helm-autoresize-min-height 20
      helm-autoresize-max-height helm-autoresize-min-height)
(helm-autoresize-mode 1)

(helm-mode 1)

(require 'helm-bufler)
(global-set-key (kbd "C-x b") #'(lambda () (interactive) (helm :sources '(helm-bufler-source))))
;;

;; gdscript-mode
(require 'gdscript-mode)
;;

;; go-translate
(require 'go-translate)
(global-set-key (kbd "C-c C-S-t") 'gt-do-translate)
(setq gt-langs '(en ar))
(setq gt-default-translator
      (gt-translator
       :taker (gt-taker :text 'buffer :pick 'paragraph)
       :engines (list (gt-google-engine))
       :render (gt-buffer-render)))
;;

;; lsp-mode
(require 'lsp-mode)

(setq lsp-keymap-prefix "C-c C-l")
(add-hook 'prog-mode-hook #'lsp)
;;
