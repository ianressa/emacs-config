(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(add-hook 'yaml-mode-hook #'display-line-numbers-mode)
(add-hook 'docker-compose-mode-hook #'display-line-numbers-mode)

(setq preferred-bg-alpha 90)
;(set-frame-parameter nil 'alpha-background preferred-bg-alpha)
;(add-to-list 'default-frame-alist (cons 'alpha-background preferred-bg-alpha))

(defun toggle-alpha-background ()
  (interactive)
  (if (eq (frame-parameter nil 'alpha-background) 100)
      (set-frame-parameter nil 'alpha-background preferred-bg-alpha)
    (set-frame-parameter nil 'alpha-background 100)))

(load-theme 'modus-operandi-tinted t)

(set-face-attribute 'default nil
		    :family "Liberation Mono"
		    :weight 'normal
		    :width 'normal
		    :height 90)

(set-face-attribute 'fixed-pitch nil
		    :family "Liberation Mono"
		    :weight 'normal
		    :width 'normal
		    :height 90)

(set-face-attribute 'variable-pitch nil
		    :family "ETBookOT"
		    :weight 'normal
		    :width 'normal
		    :height 120)

(custom-theme-set-faces
 'user
 `(org-level-4 ((t (:inherit variable-pitch :height 1.1))))
 `(org-level-3 ((t (:inherit variable-pitch :height 1.25))))
 `(org-level-2 ((t (:inherit variable-pitch :height 1.5))))
 `(org-level-1 ((t (:inherit variable-pitch :height 1.75))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-link ((t (:foreground "royal blue" :underline t))))
 '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))
