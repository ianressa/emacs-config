(add-hook 'org-mode-hook 'variable-pitch-mode)
(add-hook 'org-mode-hook 'org-superstar-mode)
(add-hook 'org-mode-hook 'visual-line-mode)

(setq org-hide-emphasis-markers t)

(setq visual-fill-column-width 110)
(setq org-image-actual-width '(500))
(setq org-startup-with-inline-images t)

(setq org-log-done 'time)
(setq org-todo-keywords
      '((sequence "TODO(t)"
		  "NEXT(n)"
		  "START(s)"
		  "WAIT(w)"
		  "DONE(d)"
		  "CANCEL(c)")))
(setq org-agenda-custom-commands
      '(("c" "Simple agenda view"
	 ((agenda "")
	  (alltodo "")))))
(setq org-capture-templates
      '(("t" "TODO")
	))

(defun org-insert-date ()
  (interactive)
  (require 'org)
  (insert (format-time-string "%m-%d-%Y" (org-read-date nil 'to-time nil "Date: "))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
	       '("IEEEtran" "\\documentclass[10pt,conference]{IEEEtran}"
		  ("\\section{%s}" . "\\section*{%s}")
		  ("\\subsection{%s}" . "\\subsection*{%s}")
		  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
		  ("\\paragraph{%s}" . "\\paragraph*{%s}")
		  ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(with-eval-after-load 'custom
  (setq
   org-directory "~/.org/"
   org-agenda-files (list org-directory)
   org-modules '(ol-bbdb ol-bibtex ol-docview ol-eww ol-gnus org-habit ol-info ol-irc ol-mhe ol-rmail ol-w3m)))
