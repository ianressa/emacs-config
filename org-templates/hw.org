#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [a4paper,12pt]
#+LATEX_HEADER: \usepackage[margin=0.75in]{geometry}
#+LATEX_HEADER: \usepackage{sectsty}
#+LATEX_HEADER: \sectionfont{\large}
#+LATEX_HEADER: \subsectionfont{\normalsize}
#+OPTIONS: toc:nil
#+AUTHOR: Ian Ressa
#+EMAIL: ianressa@umich.edu
